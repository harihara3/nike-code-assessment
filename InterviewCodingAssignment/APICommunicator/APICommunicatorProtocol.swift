//
//  APICommunicatorProtocol.swift
//  InterviewCodingAssignment
//
//  Created by Hari Manchalla on 8/16/20.
//  Copyright © 2020 Standard. All rights reserved.
//

import Foundation

protocol APICommunicatorProtocol {
    func fetchAlbumList(completion: @escaping (_ response: AlbumResponse?, _ error: Error?) -> ())
}
