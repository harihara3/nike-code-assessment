//
//  Genre.swift
//  InterviewCodingAssignment
//
//  Created by Hari Manchalla on 8/16/20.
//  Copyright © 2020 Standard. All rights reserved.
//

import Foundation

struct Genre: Codable {
    let genreId: String
    let name: String
}
