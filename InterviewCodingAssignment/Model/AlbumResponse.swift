//
//  AlbumResponse.swift
//  InterviewCodingAssignment
//
//  Created by Hari Manchalla on 8/16/20.
//  Copyright © 2020 Standard. All rights reserved.
//

import Foundation

struct AlbumResponse: Codable {
    let feed: Feed
}

struct Feed: Codable {
    let id: String
    let results: [Album]
}
