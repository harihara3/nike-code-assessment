//
//  AlbumListViewModel.swift
//  InterviewCodingAssignment
//
//  Created by Hari Manchalla on 8/16/20.
//  Copyright © 2020 Standard. All rights reserved.
//

import Foundation

class AlbumListViewModel {
    private var albums: [Album] = []
    var reloadData: (() -> Void)?
    var showErrorScreen: (() -> Void)?
    var communicator: APICommunicatorProtocol = APICommunicator()

    init() { }

    var numberOfRows: Int {
        return albums.count
    }

    func dataForRow(at index: Int) -> Album? {
        guard albums.count > index else { return nil }
        
        return albums[index]
    }

    func viewLoaded() {
        fetchAlbumList()
    }

    private func fetchAlbumList() {
        communicator.fetchAlbumList { (response, error) in
            guard error == nil, let unwrappedResponse = response else {
                self.showErrorScreen?()
                return
            }
            self.albums = unwrappedResponse.feed.results
            self.reloadData?()
        }
    }
}
