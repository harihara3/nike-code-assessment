//
//  Album.swift
//  InterviewCodingAssignment
//
//  Created by Hari Manchalla on 8/16/20.
//  Copyright © 2020 Standard. All rights reserved.
//

import Foundation

struct Album: Codable {
    let id: String
    let artistId: String
    let artistName: String
    let name: String
    let artworkUrl100: String
    let releaseDate: String
    let copyright: String?
    let genres: [Genre]
    let url: String
}
