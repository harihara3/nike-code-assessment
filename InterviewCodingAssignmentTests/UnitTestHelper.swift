//
//  UnitTestHelper.swift
//  InterviewCodingAssignmentTests
//
//  Created by Hari Manchalla on 8/16/20.
//  Copyright © 2020 Standard. All rights reserved.
//

import Foundation

class UnitTestHelper {
    public static func getData(fromFileWithName name: String, bundle: Bundle) -> Data {
      guard let path = bundle.path(forResource: name, ofType: "json") else {
        return Data()
      }
      let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
      return data ?? Data()
    }

    static func getJsonDecodeObject<T: Decodable>(type: T.Type, jsonFileName: String, bundle: Bundle) -> T?  {
        let jsonData = getData(fromFileWithName: jsonFileName, bundle: bundle)
        do {
            let jsonObject = try JSONDecoder().decode(T.self, from: jsonData)
            return jsonObject
        } catch _ {
            return nil
        }
    }
}
