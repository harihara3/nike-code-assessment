//
//  WebViewController.swift
//  InterviewCodingAssignment
//
//  Created by Hari Manchalla on 8/16/20.
//  Copyright © 2020 Standard. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate {
    var webView: WKWebView?
    var urlString = ""

    override func loadView() {
        webView = WKWebView()
        webView?.navigationDelegate = self
        view = webView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if let url = URL(string: urlString) {
            webView?.load(URLRequest(url: url))
            webView?.allowsBackForwardNavigationGestures = true
        }
    }
}
