//
//  AlbumListViewModelTests.swift
//  InterviewCodingAssignmentTests
//
//  Created by Hari Manchalla on 8/16/20.
//  Copyright © 2020 Standard. All rights reserved.
//

import XCTest

@testable import InterviewCodingAssignment

class AlbumListViewModelTests: XCTestCase {
    var viewModel = AlbumListViewModel()
    var responseExpectation = XCTestExpectation(description: "responseExpectation")
    var errorExpectation = XCTestExpectation(description: "errorExpectation")

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testWithValidData() {
        viewModel.communicator = MockAPICommunicator()
        viewModel.reloadData = {
            self.responseExpectation.fulfill()
        }
        viewModel.viewLoaded()
        wait(for: [responseExpectation], timeout: 10)
        XCTAssertEqual(viewModel.numberOfRows, 100)
        let album = viewModel.dataForRow(at: 0)
        XCTAssertNotNil(album)
        XCTAssertEqual(album?.id, "1525932943")
        XCTAssertEqual(album?.artistId, "458140076")
        XCTAssertEqual(album?.artistName, "Young Dolph")
        XCTAssertEqual(album?.name, "Rich Slave")
        XCTAssertEqual(album?.artworkUrl100, "https://is1-ssl.mzstatic.com/image/thumb/Music124/v4/f4/67/7e/f4677ee9-2cc6-7c42-d47d-0332467bf248/194690167119_cover.jpg/200x200bb.png")
        XCTAssertEqual(album?.releaseDate, "2020-08-14")
        XCTAssertEqual(album?.copyright, "℗ 2020 Paper Route EMPIRE")
        XCTAssertEqual(album?.url, "https://music.apple.com/us/album/rich-slave/1525932943?app=music")
        XCTAssertNil(viewModel.dataForRow(at: 100))
    }

    func testWithEmptyData() {
        viewModel.communicator = MockEmptyAPICommunicator()
        viewModel.reloadData = {
            self.responseExpectation.fulfill()
        }
        viewModel.viewLoaded()
        wait(for: [responseExpectation], timeout: 10)
        XCTAssertEqual(viewModel.numberOfRows, 0)
        XCTAssertNil(viewModel.dataForRow(at: 0))
    }

    func testError() {
        viewModel.communicator = MockErrorAPICommunicator()
        viewModel.showErrorScreen = {
            self.errorExpectation.fulfill()
        }
        viewModel.viewLoaded()
        wait(for: [errorExpectation], timeout: 10)
        XCTAssertEqual(viewModel.numberOfRows, 0)
        XCTAssertNil(viewModel.dataForRow(at: 0))
    }

    class MockAPICommunicator: APICommunicatorProtocol {
        var albumResponse: AlbumResponse?

        func fetchAlbumList(completion: @escaping (_ response: AlbumResponse?, _ error: Error?) -> ()) {
            let albumResponse = UnitTestHelper.getJsonDecodeObject(type: AlbumResponse.self, jsonFileName: "AlbumResponse", bundle: Bundle(for: AlbumListViewModelTests.self))
            completion(albumResponse, nil)
        }
    }

    class MockEmptyAPICommunicator: APICommunicatorProtocol {
        var albumResponse: AlbumResponse?

        func fetchAlbumList(completion: @escaping (_ response: AlbumResponse?, _ error: Error?) -> ()) {
            let albumResponse = UnitTestHelper.getJsonDecodeObject(type: AlbumResponse.self, jsonFileName: "AlbumEmptyResponse", bundle: Bundle(for: AlbumListViewModelTests.self))
            completion(albumResponse, nil)
        }
    }

    class MockErrorAPICommunicator: APICommunicatorProtocol {
        var albumResponse: AlbumResponse?

        func fetchAlbumList(completion: @escaping (_ response: AlbumResponse?, _ error: Error?) -> ()) {
            completion(nil, NSError())
        }
    }
}


