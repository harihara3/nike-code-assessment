//
//  APICLient.swift
//  SampleInterview
//
//  Created by Hari Hara on 10/24/2019.
//  Copyright © 2019 Hari Hara. All rights reserved.
//

import UIKit

enum HTTPMethod: String {
    case get = "GET"
    case put = "PUT"
    case post = "POST"
    case delete = "DELETE"
}

enum APIError: Error {
    case invalidURL
    case requestFailed
}

struct APIClient {
    
    typealias APIClientCompletion = (HTTPURLResponse?, Data?, APIError?) -> Void
    typealias APIClientDownloadCompletion = (HTTPURLResponse?, UIImage?, APIError?) -> Void
    
    private let session = URLSession.shared
    private let baseURL = URL(string: "https://rss.itunes.apple.com/api")
    
    func request(method: HTTPMethod, path: String, _ completion: @escaping APIClientCompletion) {
        guard let url = baseURL?.appendingPathComponent(path) else {
            completion(nil, nil, .invalidURL)
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(nil, nil, .requestFailed)
                return
            }
            completion(httpResponse, data, nil)
        }
        task.resume()
    }
    
    func download(method: HTTPMethod, urlString: String, _ completion: @escaping APIClientDownloadCompletion) -> URLSessionDataTask? {
        guard let url = URL(string: urlString) else {
            completion(nil, nil, .invalidURL)
            return nil
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let httpResponse = response as? HTTPURLResponse, let mimeType = httpResponse.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else {
                    completion(nil, nil, .requestFailed)
                    return
            }
            
            completion(httpResponse, image, nil)
        }
        task.resume()
        return task
    }
}
