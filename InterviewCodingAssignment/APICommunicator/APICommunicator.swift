//
//  APICommunicator.swift
//  InterviewCodingAssignment
//
//  Created by Hari Manchalla on 8/16/20.
//  Copyright © 2020 Standard. All rights reserved.
//

import UIKit

struct APICommunicator: APICommunicatorProtocol {
    func fetchAlbumList(completion: @escaping (_ response: AlbumResponse?, _ error: Error?) -> ()) {
        APIClient().request(method: .get, path: "/v1/us/apple-music/top-albums/all/100/explicit.json") { (response, data, error) in
            guard error == nil, let unwrappedData = data else {
                completion(nil, error)
                return
            }

            do {
                let albumResponse = try JSONDecoder().decode(AlbumResponse.self, from: unwrappedData)
                completion(albumResponse, nil)
            } catch {
                completion(nil, error)
            }
        }
    }
}
