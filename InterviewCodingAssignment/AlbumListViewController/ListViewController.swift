//
//  ListViewController.swift
//  InterviewCodingAssignment
//
//  Created by Hari Manchalla on 8/16/20.
//  Copyright © 2020 aStandard. All rights reserved.
//

import UIKit

final class ListViewController: UIViewController {
    private var tableView: UITableView?
    private var activityIndicatorView: UIActivityIndicatorView?
    private let viewModel = AlbumListViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setup()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.backgroundColor = .white
        title = "Album List"
    }

    private func setupUI() {
        setupTableView()
        let activityIndicatorView = UIActivityIndicatorView(style: .large)
        self.activityIndicatorView = activityIndicatorView
        self.activityIndicatorView?.startAnimating()
        view.addSubview(activityIndicatorView)
        self.activityIndicatorView?.translatesAutoresizingMaskIntoConstraints = false
        self.activityIndicatorView?.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        self.activityIndicatorView?.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }

    private func setupTableView() {
        let tableView = UITableView()
        self.tableView = tableView
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        view.addSubview(tableView)
        self.tableView?.translatesAutoresizingMaskIntoConstraints = false
        self.tableView?.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        self.tableView?.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        self.tableView?.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        self.tableView?.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        registerCells()
    }

    private func setup() {
        self.tableView?.isHidden = true
        viewModel.viewLoaded()
        viewModel.reloadData = { [weak self] in
            DispatchQueue.main.async {
                self?.activityIndicatorView?.stopAnimating()
                self?.tableView?.isHidden = false
                self?.tableView?.reloadData()
            }
        }
    }

    private func registerCells() {
        self.tableView?.register(AlbumTableViewCell.self, forCellReuseIdentifier: "AlbumCell")
    }
}

extension ListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfRows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let album = viewModel.dataForRow(at: indexPath.row),
            let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumCell", for: indexPath) as? AlbumTableViewCell else {
                return UITableViewCell()
        }
        cell.setData(for: album)
        return cell
    }
}

extension ListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController = AlbumDetailViewController()
        viewController.album = viewModel.dataForRow(at: indexPath.row)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
