//
//  AlbumDetailViewController.swift
//  InterviewCodingAssignment
//
//  Created by Hari Manchalla on 8/16/20.
//  Copyright © 2020 Standard. All rights reserved.
//

import UIKit

class AlbumDetailViewController: UIViewController {
    private var albumName: UILabel?
    private var artistName: UILabel?
    private var genre: UILabel?
    private var releaseDate: UILabel?
    private var copyrightInfo: UILabel?
    private var albumImageView: UIImageView?
    private var iTunesStoreButton: UIButton?

    private var currentRequest: URLSessionDataTask?
    var album: Album?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Album Detail"
        setupUI()
        setupData()
    }

    private func setupUI() {
        self.view.backgroundColor = .white

        let imageView = UIImageView()
        albumImageView = imageView
        view.addSubview(imageView)
        albumImageView?.translatesAutoresizingMaskIntoConstraints = false
        albumImageView?.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        albumImageView?.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        albumImageView?.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        albumImageView?.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.7).isActive = true

        let albumName = UILabel()
        albumName.font = UIFont(name: "HelveticaNeue-Bold", size: 14)
        self.albumName = albumName

        let artistName = UILabel()
        artistName.font = UIFont(name: "HelveticaNeue-Light", size: 14)
        self.artistName = artistName

        let genre = UILabel()
        genre.font = UIFont(name: "HelveticaNeue-Light", size: 12)
        self.genre = genre

        let releaseDate = UILabel()
        releaseDate.font = UIFont(name: "HelveticaNeue-Light", size: 12)
        self.releaseDate = releaseDate

        let copyrightInfo = UILabel()
        copyrightInfo.font = UIFont(name: "HelveticaNeue-Light", size: 12)
        self.copyrightInfo = copyrightInfo

        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 5

        stackView.insertArrangedSubview(albumName, at: 0)
        stackView.insertArrangedSubview(artistName, at: 1)
        stackView.insertArrangedSubview(genre, at: 2)
        stackView.insertArrangedSubview(releaseDate, at: 3)
        stackView.insertArrangedSubview(copyrightInfo, at: 4)
        view.addSubview(stackView)

        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 20).isActive = true
        stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true


        let button = UIButton()
        iTunesStoreButton = button
        view.addSubview(button)
        iTunesStoreButton?.translatesAutoresizingMaskIntoConstraints = false
        iTunesStoreButton?.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20).isActive = true
        iTunesStoreButton?.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        iTunesStoreButton?.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        iTunesStoreButton?.heightAnchor.constraint(equalToConstant: 36).isActive = true
    }

    private func setupData() {
        downloadImage(in: album?.artworkUrl100 ?? "")
        albumName?.text = album?.name
        artistName?.text = "Artist Name: \(album?.artistName ?? "")"
        genre?.text = "Genres: \(album?.genres.compactMap { $0.name }.joined(separator: ", ") ?? "")"
        releaseDate?.text = "Release Date: \(album?.releaseDate ?? "")"
        copyrightInfo?.text = "Copyright: \(album?.copyright ?? "")"

        iTunesStoreButton?.setTitle("iTunes store", for: .normal)
        iTunesStoreButton?.setTitleColor(.black, for: .normal)
        iTunesStoreButton?.layer.cornerRadius = 18
        iTunesStoreButton?.layer.borderColor = UIColor.black.cgColor
        iTunesStoreButton?.layer.borderWidth = 1
        iTunesStoreButton?.addTarget(self, action: #selector(iTunesStoreButtonClicked(_:)), for: .touchUpInside)
    }

    private func downloadImage(in path: String) {
        currentRequest = APIClient().download(method: .get, urlString: path, { (response, image, error) in
            DispatchQueue.main.async {
                self.albumImageView?.image = image
            }
        })
    }

    @objc private func iTunesStoreButtonClicked(_ sender: UIButton) {
        let viewController = WebViewController()
        viewController.urlString = album?.url ?? ""
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
