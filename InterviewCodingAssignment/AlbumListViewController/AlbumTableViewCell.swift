//
//  AlbumTableViewCell.swift
//  InterviewCodingAssignment
//
//  Created by Hari Manchalla on 8/16/20.
//  Copyright © 2020 Standard. All rights reserved.
//

import UIKit

class AlbumTableViewCell: UITableViewCell {
    private var currentRequest: URLSessionDataTask?
    private var albumName: UILabel?
    private var artistName: UILabel?
    private var thumbnailImageView: UIImageView?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        setupUI()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
        if currentRequest != nil {
            currentRequest?.cancel()
            currentRequest = nil
        }
        self.thumbnailImageView?.image = nil
    }

    private func setupUI() {
        let imageView = UIImageView()
        self.thumbnailImageView = imageView
        self.contentView.addSubview(imageView)
        self.thumbnailImageView?.translatesAutoresizingMaskIntoConstraints = false
        self.thumbnailImageView?.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive = true
        self.thumbnailImageView?.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10).isActive = true
        self.thumbnailImageView?.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16).isActive = true
        self.thumbnailImageView?.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.thumbnailImageView?.widthAnchor.constraint(equalToConstant: 50).isActive = true

        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 5

        let albumName = UILabel()
        albumName.font = UIFont(name: "HelveticaNeue-Medium", size: 14)
        self.albumName = albumName

        let artistName = UILabel()
        artistName.font = UIFont(name: "HelveticaNeue-Light", size: 14)
        self.artistName = artistName

        stackView.insertArrangedSubview(albumName, at: 0)
        stackView.insertArrangedSubview(artistName, at: 1)
        self.contentView.addSubview(stackView)

        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 20).isActive = true
        stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16).isActive = true
        stackView.centerYAnchor.constraint(equalTo: imageView.centerYAnchor).isActive = true
    }

    func setData(for album: Album) {
        albumName?.text = album.name
        artistName?.text = album.artistName
        downloadImage(in: album.artworkUrl100)
    }

    private func downloadImage(in path: String) {
        currentRequest = APIClient().download(method: .get, urlString: path, { (response, image, error) in
            DispatchQueue.main.async {
                self.thumbnailImageView?.image = image
            }
        })
    }
}
